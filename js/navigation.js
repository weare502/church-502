/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens and enables tab
 * support for dropdown menus.
 */
( function( $ ) {

	$('#info-panel-toggle').on('click', function(){
		$('#info-panel').addClass('open');
		$('#page').addClass('panel-open');
	});

	$('#info-panel .inside').on('click', function(e){
		e.stopPropagation();
	});

	$('#info-panel').on('click', function(){
		$('#info-panel').removeClass('open');
		$('#page').removeClass('panel-open');
	});

	$('#info-panel .close').on('click', function(){
		$('#info-panel').removeClass('open');
		$('#page').removeClass('panel-open');
	});

	$('#page').fitVids();

	//Fix z-index youtube video embedding
    $('iframe').each(function() {
      var url = $(this).attr("src");
      if (url.indexOf("?") > 0) {
        $(this).attr({
          "src" : url + "&wmode=transparent",
          "wmode" : "opaque"
        });
      }
      else {
        $(this).attr({
          "src" : url + "?wmode=transparent",
          "wmode" : "opaque"
        });
      }
    });

	$('.menu-toggle').on('click', function(){
		$('#primary-menu').toggleClass('open');
		$('body').toggleClass('menu-open');
	});

	var resizeTimer;
	$(window).on('resize', function(){
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			if ( $('body').hasClass('menu-open') && ( 999 < $( window ).width() ) ){
				$('body').removeClass('menu-open');
				$('#primary-menu').removeClass('open');
			}
		}, 250);
	});
	
} )( jQuery );

