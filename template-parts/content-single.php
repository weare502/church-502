<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Church
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php 	$header_bg_img = get_option( 'church502_generic_header_image' ); 
			$header_bg_img = wp_get_attachment_image_src( $header_bg_img, 'large', false ); 
	?>
	<style type="text/css">
		.entry-header {
			background-image: url(<?php echo $header_bg_img[0]; ?>);
		}
	</style>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php church_502_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_post_thumbnail(); ?>
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'church-502' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php if ( get_post_type( $post ) === 'post' ) : ?>
			<div class="author-bio">
				<div class="avatar">
					<?php echo get_avatar( get_the_author_id(), '160', get_the_author_meta( 'display_name' ) ); ?>
				</div>
				<div class="information">
					<div class="display-name">
						<?php the_author_meta('display_name'); ?>
					</div>
					<div class="author-description">
						<?php the_author_meta( 'description' ); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php church_502_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

