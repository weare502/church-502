<?php
/**
 * Church functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Church
 */

if ( ! function_exists( 'church_502_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function church_502_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Church, use a find and replace
	 * to change 'church-502' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'church-502', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'church-502' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

}
endif; // church_502_setup
add_action( 'after_setup_theme', 'church_502_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function church_502_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'church_502_content_width', 768 );
}
add_action( 'after_setup_theme', 'church_502_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function church_502_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'church-502' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
// add_action( 'widgets_init', 'church_502_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function church_502_scripts() {
	wp_enqueue_style( 'church-502-style', get_stylesheet_uri(), array(), '20150002' );

	wp_enqueue_script( 'church-502-fitvids', get_template_directory_uri() . '/bower_components/fitvids/jquery.fitvids.js', array( 'jquery' ), '20120206', true );
	
	wp_enqueue_script( 'church-502-navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery' ), '20120209', true );

	wp_enqueue_script( 'church-502-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'church_502_scripts' );

add_action( 'admin_init', function(){

	wp_register_script( 'church-502-fitvids', get_template_directory_uri() . '/bower_components/fitvids/jquery.fitvids.js', array( 'jquery' ), '20120206', true );
	
	add_editor_style( get_stylesheet_directory_uri() . '/editor-style.css' );
	
});

add_action( 'admin_enqueue_scripts', function() {

	wp_enqueue_script( 'church-502-fitvids' );

});

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * TinyMCE Custom Formats & Mods
 */
require get_template_directory() . '/inc/tinymce.php';

/**
 * Super Simple Podcasting Mods
 */
require get_template_directory() . '/inc/podcast.php';

/**
 * Register ACF Options Page for Visitor Info Pop-Over
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Visitor Info Settings',
		'menu_title'	=> 'Visitor Info Settings',
		'parent_slug'	=> 'themes.php'
	));

}