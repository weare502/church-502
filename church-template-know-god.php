<?php
/**
 * Template Name: Know God Template
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Church
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php $header_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large', false ); ?>

				<?php if ( $header_image ) : ?>

					<style type="text/css">
						.page .entry-header {
							background-color: transparent !important;
							background-image: url(<?php echo $header_image[0]; ?>);
						}
					</style>

				<?php endif; ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
					</div><!-- .entry-content -->



					<?php $know_form = get_field('know_form'); ?>
					
					<?php if ( $know_form ) : ?>

						<?php $know_form_bg = get_field('know_form_bg'); ?>
					
						<div class="know-form clear" style="background-image: url(<?php echo $know_form_bg['sizes']['large']; ?>);">
							
							<?php $know_id = $know_form['id']; ?>
							
							<div class="know-form-description">
								<?php echo $know_form['description']; ?>
							</div>

							<?php echo do_shortcode("[gravityform id='${know_id}' title='false' description='false']"); ?>

						</div>

					<?php endif; ?>

					<?php $life_issues = get_field('know_life_issues'); ?>

					<?php if ( $life_issues ) : ?>

						<h2 class="life-issues-title">Life Issues</h2>

						<?php if ( have_rows('know_life_issues') ) : ?>
						
							<div class="life-issues">
							
								<?php while ( have_rows('know_life_issues' ) ) : the_row(); ?>	

									<div class="issue">
										<div class="issue-title"><?php the_sub_field('title'); ?></div>
										<p class="issue-description"><?php the_sub_field('description'); ?></p>
										<a href="<?php the_sub_field('link'); ?>" class="issue-link">Read More</a>
									</div>

								<?php endwhile; ?>

							</div>

						<?php endif; ?>

					<?php endif; ?>

					<footer class="entry-footer">
						<?php
							edit_post_link(
								sprintf(
									/* translators: %s: Name of current post */
									esc_html__( 'Edit %s', 'church-502' ),
									the_title( '<span class="screen-reader-text">"', '"</span>', false )
								),
								'<span class="edit-link">',
								'</span>'
							);
						?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
