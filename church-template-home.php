<?php
/**
 * Template Name: Home Page Template
 */

get_header(); ?>

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/countdown/2.6.0/countdown.js"></script> -->
<!-- <script type="text/javascript">
( function( $ ){

	var nextSunday = Date.parse('next sunday').add({ hours : 10, minutes : 15 });
	var count;
	var timer = window.setInterval(function(){
		count = countdown(null, nextSunday);
		updateCountdown(count);
	}, 1000);

	function updateCountdown( count ){
		var $days = $('.countdown-clock .days .value');
		var $hours = $('.countdown-clock .hours .value');
		var $minutes = $('.countdown-clock .minutes .value');
		var $seconds = $('.countdown-clock .seconds .value');
		var $dayPlural = $('.countdown-clock .days .s');
		var $hourPlural = $('.countdown-clock .hours .s');
		var $minutePlural = $('.countdown-clock .minutes .s');
		var $secondPlural = $('.countdown-clock .seconds .s');

		$days.text(count.days);
		count.days == 1 ? $dayPlural.hide() : $dayPlural.show();

		$hours.text(count.hours);
		count.hours == 1 ? $hourPlural.hide() : $hourPlural.show();
			
		$minutes.text(count.minutes);
		count.minutes == 1 ? $minutePlural.hide() : $minutePlural.show();

		$seconds.text(count.seconds);
		count.seconds == 1 ? $secondPlural.hide() : $secondPlural.show();
	}

})(jQuery); -->
	

<!-- </script> -->
<!-- <div class="wide-wrap hide-on-mobile">
	<div class="countdown-clock full-width">
		<div class="tagline">Our Next<br> <strong>Sunday Service</strong></div>
		<div class="days time"><span class="value"></span> Day<span class="s">s</span></div>
		<div class="hours time"><span class="value"></span>Hour<span class="s">s</span></div>
		<div class="minutes time"> <span class="value"></span>Minute<span class="s">s</span></div>
		<div class="seconds time"><span class="value"></span>Second<span class="s">s</span></div>
	</div>
</div> -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php $hero_img = get_field('home_entry_bg'); ?>
				<?php if ( $hero_img ) : ?>
					<style type="text/css">
						.entry-header {
							background-image: url(<?php echo $hero_img['sizes']['large']; ?>);
						}
					</style>
				<?php endif; ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?php the_field('home_entry_header'); ?></h1>
						<div class="entry-paragraph">
							<p><?php the_field('home_entry_paragraph') ?></p>
						</div>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<div class="mission-statement full-width">
							<?php the_field('home_mission_statement'); ?>
						</div>

						<div class="meet-the-pastor">
							<div class="pastor-headshot">
								<?php $headshot = get_field('home_pastor_headshot'); ?>
								<?php if ( $headshot ) : ?>
									<img src="<?php echo $headshot['sizes']['medium']; ?>" alt="<?php echo $headshot['alt']; ?>" />
								<?php endif; ?>
							</div>
							<div class="pastor-short-bio">
								<?php the_field('home_meet_the_pastor'); ?>
							</div>
						</div>

						<?php if ( have_rows( 'home_banners' ) ) : ?>

							<div class="banners">

							<?php while ( have_rows( 'home_banners' ) ) : the_row(); ?>
								<?php $bg_image = get_sub_field('bg_image'); ?>
								<a href="<?php the_sub_field('link'); ?>" class="banner" style="background-image: url(<?php echo $bg_image['sizes']['large']; ?>);">
									<h4 class="banner-title"><?php the_sub_field('text'); ?></h4>
									<p class="read-more button light-color">Read More</p>
								</a>
							<?php endwhile; ?>

							</div>

						<?php endif; ?>

						<div class="call-to-actions">

							<?php if ( have_rows( 'home_cta' ) ) : ?>

								<?php while ( have_rows( 'home_cta' ) ) : the_row(); ?>

									<div class="call-to-action">
										<i class="cta-icon fa <?php the_sub_field('icon'); ?>"></i>
										<h3 class="cta-title"><?php the_sub_field('title'); ?></h3>
										<p class="cta-description"><?php the_sub_field('description'); ?></p>
										<a href="<?php the_sub_field('link'); ?>" class="button">More</a>
									</div>

								<?php endwhile; ?>

							<?php endif; ?>

						</div>

						<div class="upcoming-event">
							<?php 
								if ( function_exists('tribe_get_events') ) :
									$events = tribe_get_events( array( 
										'posts_per_page' => 1,
										'start_date' => current_time('yyyy-mm-dd'),
									) );

								global $post;

								foreach ( $events as $post ) :
									setup_postdata( $post );
									
									echo tribe_event_featured_image(); ?>
									<div class="event-info">
										<h4 class="event-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
										<p class="event-start"><?php echo tribe_get_start_date(); ?></p>
										<p class="event-description"><?php the_excerpt(); ?></p>
									</div>
								
								<?php endforeach;

								wp_reset_postdata();

								endif; ?>
						</div>
						<?php the_content(); ?>
					</div><!-- .entry-content -->

					<footer class="entry-footer">

					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>