<?php
/**
 * Template Name: Contact Template
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Church
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php $header_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large', false ); ?>

				<?php if ( $header_image ) : ?>

					<style type="text/css">
						.page .entry-header {
							background-color: transparent !important;
							background-image: url(<?php echo $header_image[0]; ?>);
						}
					</style>

				<?php endif; ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<div class="entry-content">
					<div class="full-width light-gray clear">
							<div class="half">
								<?php $phone = get_field('contact_phone'); ?>
								<?php $email = get_field('contact_email'); ?>
								<?php if ( $phone ) : ?>
									<h4>Phone</h4>
									<p><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></p>
								<?php endif; ?>
								<?php if ( $email ) : ?>
									<h4>Email</h4>
									<p><a href="mailto:<?php echo antispambot( $email ); ?>"><?php echo antispambot( $email ); ?></a></p>
								<?php endif; ?>
							</div>
							<div class="half">
								<?php $hours = get_field('contact_office_hours'); ?>
								<?php $address = get_field('contact_address'); ?>
								<?php if ( $hours ) : ?>
									<h4>Office Hours</h4>
									<p><?php echo $hours; ?></p>
								<?php endif; ?>
								<?php if ( $address ) : ?>
									<h4>Address</h4>
									<p><?php echo $address; ?></p>
								<?php endif; ?>
							</div>
						</div>
						<?php the_content(); ?>
					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php
							edit_post_link(
								sprintf(
									/* translators: %s: Name of current post */
									esc_html__( 'Edit %s', 'church-502' ),
									the_title( '<span class="screen-reader-text">"', '"</span>', false )
								),
								'<span class="edit-link">',
								'</span>'
							);
						?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // End of the loop. ?>

			<?php 
			$map_address = urlencode( get_option( 'church502_address', '' ) );
			$map_url = "https://maps.googleapis.com/maps/api/staticmap?center={$map_address}&zoom=14&size=1600x400&scale=2&markers={$map_address}"; ?>

			<style type="text/css">
				.contact-map {
					background-image: url(<?php echo $map_url; ?>);
				}
			</style>

			<div class="contact-map" height="500">


			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
