<?php
/**
 * Template Name: Events Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Church
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">

				<?php 	$header_bg_img = get_option( 'church502_generic_header_image' ); 
						$header_bg_img = wp_get_attachment_image_src( $header_bg_img, 'large', false ); 
				?>
				<style type="text/css">
					.page-header {
						background-image: url(<?php echo $header_bg_img[0]; ?>);
					}
				</style>

				<?php wp_reset_postdata(); ?>
				<?php

					echo '<h1 class="page-title">';
					echo is_singular() ? get_the_title($post->ID) : post_type_archive_title( '', false );
					echo '</h1>';
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php if ( is_archive() ) : ?>

				<p class="event-cats">

				<?php $event_cats = get_terms('tribe_events_cat');

				foreach ( $event_cats as $cat ){
					$url = get_term_link( $cat );
					echo "<a class='button' href='{$url}'>{$cat->name}</a>";
				}

				?>

				</p>

			<?php endif; ?>

			<?php if ( is_archive() && ! is_tax() ) : ?>
				<p class="sunday-events-notice"><?php echo get_option( 'church502_events_notice' ); ?></p>
			<?php endif; ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>