<?php 

function church_add_video_embed_note($return) {
	if ( is_admin() )
    	return "<div class='video-embed'>{$return}</div>";
    else
    	return $return;
}
add_filter( 'embed_oembed_html', 'church_add_video_embed_note', 10, 3);

// Callback function to insert 'styleselect' into the $buttons array
function tiak_mce_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	//var_dump($buttons);
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'tiak_mce_buttons');

// Callback function to filter the MCE settings
function tiak_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Button',  
			'selector' => 'a',  
			'classes' => 'button',
			'icon'	   => ' fa fa-hand-pointer-o'
		),  
		array(  
			'title' => 'Horizontal Rule',  
			// 'selector' => 'p',
			'block' => 'hr',  
			'classes' => 'hr',
			'icon'	   => ' fa fa-ellipsis-h'
		),
		array(  
			'title' => 'H.R. Primary Color',  
			'selector' => 'hr',  
			'classes' => 'hr primary-color-hr',
			// 'icon'	   => ' fa fa-ellipsis-h'
		),
		array(  
			'title' => 'Full Width',  
			'selector' => 'p',  
			'classes' => 'full-width',
			'icon'	   => ' fa fa-arrows-h'
		),
		array(  
			'title' => 'Primary Color Background',  
			'selector' => 'p',  
			'classes' => 'primary-color',
			// 'icon'	   => ' fa fa-circle-thin'
		),
		array(  
			'title' => 'Light Gray Background',  
			'selector' => 'p',  
			'classes' => 'light-gray',
			// 'icon'	   => ' fa fa-circle-thin'
		),
		array(  
			'title' => 'Larger Text',  
			'selector' => '*',  
			'classes' => 'larger',
			'icon'	   => ' fa fa-text-height'
		),
		array(  
			'title' => 'Primary Color Text',  
			'selector' => '*',  
			'classes' => 'primary-color-text',
			'icon'	   => ' fa fa-eyedropper'
		),
		array(  
			'title' => 'Document Icon',  
			'selector' => 'a',  
			'classes' => 'document-icon',
			'icon'	   => ' fa fa-file-text'
		),
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
}
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'tiak_mce_before_init_insert_formats' ); 

add_action('admin_head', function(){
	echo "<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,700' rel='stylesheet' type='text/css'>";
});
