<?php
/**
 * Church Theme Customizer.
 *
 * @package Church
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function church_502_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_control( 'blogdescription' );
	$wp_customize->remove_control( 'display_header_text' );
	$wp_customize->remove_section( 'header_image' );
	$wp_customize->remove_section( 'colors' );

 	$wp_customize->add_setting( 'church502_address', array( 
 		'type' => 'option',
 		'transport' => 'postMessage',
 		'default' => '',
 		'sanitize_callback' => 'esc_html',
 	) );

 	$wp_customize->add_control( 'church502_address', array(
 		'type' => 'text',
 		'label' => 'Physical Address',
 		'section' => 'title_tagline',
 		'priority' => 100,
 	) );

 	$wp_customize->add_setting( 'church502_facebook', array( 
 		'type' => 'option',
 		// 'transport' => 'postMessage',
 		'default' => '',
 		'sanitize_callback' => 'esc_url',
 	) );

 	$wp_customize->add_control( 'church502_facebook', array(
 		'type' => 'url',
 		'label' => 'Facebook URL',
 		'section' => 'title_tagline',
 		'priority' => 101,
 	) );

 	$wp_customize->add_setting( 'church502_twitter', array( 
 		'type' => 'option',
 		// 'transport' => 'postMessage',
 		'default' => '',
 		'sanitize_callback' => 'esc_url',
 	) );

 	$wp_customize->add_control( 'church502_twitter', array(
 		'type' => 'url',
 		'label' => 'Twitter URL',
 		'section' => 'title_tagline',
 		'priority' => 101,
 	) );

 	$wp_customize->add_setting( 'church502_instagram', array( 
 		'type' => 'option',
 		// 'transport' => 'postMessage',
 		'default' => '',
 		'sanitize_callback' => 'esc_url',
 	) );

 	$wp_customize->add_control( 'church502_instagram', array(
 		'type' => 'url',
 		'label' => 'Instagrm Profile URL',
 		'section' => 'title_tagline',
 		'priority' => 101,
 	) );

 	$wp_customize->add_setting( 'church502_newsletter_cta', array( 
 		'type' => 'option',
 		'transport' => 'postMessage',
 		'default' => '',
 		'sanitize_callback' => 'esc_html',
 	) );

 	$wp_customize->add_control( 'church502_newsletter_cta', array(
 		'type' => 'text',
 		'label' => 'Footer Newsletter "Call to Action"',
 		'section' => 'title_tagline',
 		'priority' => 102,
 	) );

 	$wp_customize->add_setting( 'church502_newsletter_form_id', array( 
 		'type' => 'option',
 		// 'transport' => 'postMessage',
 		'default' => '',
 		'sanitize_callback' => 'esc_html',
 	) );

 	$gf_url = admin_url( 'admin.php?page=gf_edit_forms');

 	$wp_customize->add_control( 'church502_newsletter_form_id', array(
 		'type' => 'number',
 		'label' => 'Newsletter Form ID',
 		'section' => 'title_tagline',
 		'priority' => 101,
 		'description' => "This is the ID of the form used to collect email addresses. <br><a href='{$gf_url}'>List of Forms</a>",
 	) );

 	$wp_customize->add_setting( 'church502_logo', array( 
 		'type' => 'option',
 		// 'transport' => 'postMessage',
 		'default' => '',
 		// 'sanitize_callback' => 'esc_html',
 	) );

 	$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'church502_logo', array(
		'label' => __( 'Logo', 'church-502' ),
		'section' => 'title_tagline',
		'mime_type' => 'image',
 	) ) );

 	$wp_customize->add_setting( 'church502_generic_header_image', array( 
		'type' => 'option',
		// 'transport' => 'postMessage',
		'default' => '',
		// 'sanitize_callback' => 'esc_html',
 	) );

 	$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'church502_generic_header_image', array(
		'label' => __( 'Generic Header Image', 'church-502' ),
		'section' => 'title_tagline',
		'mime_type' => 'image',
		'priority' => 200,
 		'description' => 'This will be shown for any generated pages that do not allow you to set a featured image. Ie., blog index, podcast index, etc. <br><br> It will also be shown as the header for blog posts.',
 	) ) );

 	$wp_customize->add_setting( 'church502_events_notice', array (
 		'type' => 'option',
 		'transport' => 'postMessage',
 		'default' => '',
 		'sanitize_callback' => 'wp_kses_post'
 	) );

 	$wp_customize->add_control( 'church502_events_notice', array(
 		'type' => 'textarea',
 		'input_attrs' => array(
 				'rows' => '2'
 			),
 		'label' => 'Events Custom Notice',
 		'description' => 'Enter a notice to appear on the main event page. You may use basic HTML tags like <code>&lt;b&gt;</code> for bold text or <code>&lt;i&gt;</code> for italics.',
 		'section' => 'title_tagline',
 		'priority' => 85
 	) );
}
add_action( 'customize_register', 'church_502_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function church_502_customize_preview_js() {
	wp_enqueue_script( 'church_502_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'church_502_customize_preview_js' );
