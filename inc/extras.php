<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Church
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function church_502_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'church_502_body_classes' );

// Replaces the excerpt "more" text by a link
function church502_excerpt_more($more) {
    global $post;
    $moretext = 'tribe_events' === get_post_type($post) ? 'Find Out More' : 'Read More';
    $permalink = get_permalink($post->ID);
	return "...<br><a class='moretag button' href='{$permalink}'> {$moretext}</a>";
}
add_filter('excerpt_more', 'church502_excerpt_more');
