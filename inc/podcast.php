<?php 

add_filter( 'ssp_register_post_type_args', function( $args ){

	$labels = array(
		'name' => _x( 'Sermons', 'post type general name' , 'church-502' ),
		'singular_name' => _x( 'Sermon', 'post type singular name' , 'church-502' ),
		'add_new' => _x( 'Add New', 'podcast' , 'church-502' ),
		'add_new_item' => sprintf( __( 'Add New %s' , 'church-502' ), __( 'Sermon' , 'church-502' ) ),
		'edit_item' => sprintf( __( 'Edit %s' , 'church-502' ), __( 'Sermon' , 'church-502' ) ),
		'new_item' => sprintf( __( 'New %s' , 'church-502' ), __( 'Sermon' , 'church-502' ) ),
		'all_items' => sprintf( __( 'All %s' , 'church-502' ), __( 'Sermons' , 'church-502' ) ),
		'view_item' => sprintf( __( 'View %s' , 'church-502' ), __( 'Sermon' , 'church-502' ) ),
		'search_items' => sprintf( __( 'Search %a' , 'church-502' ), __( 'Sermons' , 'church-502' ) ),
		'not_found' =>  sprintf( __( 'No %s Found' , 'church-502' ), __( 'Sermons' , 'church-502' ) ),
		'not_found_in_trash' => sprintf( __( 'No %s Found In Trash' , 'church-502' ), __( 'Sermons' , 'church-502' ) ),
		'parent_item_colon' => '',
		'menu_name' => __( 'Sermons' , 'church-502' ),
		'filter_items_list' => sprintf( __( 'Filter %s list' , 'church-502' ), __( 'Sermon' , 'church-502' ) ),
		'items_list_navigation' => sprintf( __( '%s list navigation' , 'church-502' ), __( 'Sermon' , 'church-502' ) ),
		'items_list' => sprintf( __( '%s list' , 'church-502' ), __( 'Sermon' , 'church-502' ) ),
	);

	$args['labels'] = $labels;

	return $args;

}, 10, 1 );

add_filter( 'ssp_archive_slug', function( $slug ){

	$slug = 'sermons';

	return $slug;

}, 10, 1 );