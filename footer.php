<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Church
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="newsletter-signup clear">
			<div class="form-wrapper">
				<h3 class="newsletter-cta"><?php echo get_option('church502_newsletter_cta', ''); ?></h3>
				<?php $newsletter_form = intval( get_option('church502_newsletter_form_id', '') ); ?>
				<?php if ( class_exists('GFAPI') && $newsletter_form ) :

					if ( GFAPI::get_form( $newsletter_form ) )
						echo do_shortcode("[gravityform id='{$newsletter_form}' title='false' description='false' ajax='true']");

				endif; ?>
			</div>
		</div>

		<div class="site-info">
			<div class="site-title"><?php bloginfo( 'name' ); ?></div>
			<address class="address"><?php echo get_option('church502_address', ''); ?></address>
			<div class="social-links">
				<?php if ( get_option('church502_facebook') ) : ?>
					<a href="<?php echo get_option('church502_facebook'); ?>" target="_blank"><i class="fa fa-facebook social-link"></i></a>
				<?php endif; ?>
				<?php if ( get_option('church502_twitter') ) : ?>
					<a href="<?php echo get_option('church502_twitter'); ?>" target="_blank"><i class="fa fa-twitter social-link"></i></a>
				<?php endif; ?>
				<?php if ( get_option('church502_instagram') ) : ?>
					<a href="<?php echo get_option('church502_instagram'); ?>" target="_blank"><i class="fa fa-instagram social-link"></i></a>
				<?php endif; ?>
			</div>
		</div>

	</footer><!-- #colophon -->

</div><!-- #page -->

<button id="info-panel-toggle"><span class="screen-reader-text">Visitors Info</button>
<div id="info-panel">
	<div class="inside">
		<button class="close light-color"><i class="fa fa-times"></i> Close</button>
		<div class="top light-color">
			<div class="wrap">
				<?php the_field('visitor_info', 'option'); ?>
			</div>
		</div>
		<div class="bottom">
			<div class="wrap">
				<h3><?php _e('Location', 'church-502'); ?></h3>
				<address><?php echo get_option( 'church502_address', '' ); ?></address>
				<p>
					<?php $directions_address = urlencode( get_option('church502_address', '') ); ?>
					<a href="https://google.com/maps/?q=<?php echo $directions_address; ?>" class="button" target="_blank">Directions</a>
					<?php $visitor_map = get_field('visitor_map', 'option'); ?>
					<?php if ( $visitor_map ) : ?>
						<a href="<?php echo $visitor_map['sizes']['large']; ?>" class="button" target="_blank">Map of Church</a>
					<?php endif; ?>
				</p>

				<?php $visitor_contact_form = get_field('visitor_contact_form', 'option');
					if ( class_exists('GFAPI') && $visitor_contact_form ) : ?>
						<br><br>
						<h4><?php _e( 'Contact Us', 'church-502' ); ?></h4>
						<?php echo do_shortcode("[gravityform id='{$visitor_contact_form['id']}' title='false' description='false' ajax='true']");
					endif; 
				?>
			</div>
		</div>
	</div>
</div>

<?php wp_footer(); ?>

</body>
</html>
